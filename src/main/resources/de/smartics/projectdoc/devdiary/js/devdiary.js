/*
 * Copyright 2014-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-devdiary:create-doctype-template-diary',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-devdiary:create-doctype-template-year',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-devdiary:create-doctype-template-day', function(wizard) {
    wizard.on('pre-render.page1Id', function(e, state) {
        state.wizardData.selectedWebItemData = PROJECTDOC.prerenderWizard(e, state);
      });

	wizard.on("post-render.page1Id", function(e, state) {
        $('#day-date').datepicker({
            dateFormat : "yy-mm-dd"
        });
    });

    wizard.on('submit.page1Id', function(e, state) {
    	PROJECTDOC.adjustToLocation(state);
    });

    wizard.assembleSubmissionObject = PROJECTDOC.assemblePageCreationData;
});

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-devdiary:create-doctype-template-week', function(wizard) {
    wizard.on('pre-render.page1Id', function(e, state) {
        state.wizardData.selectedWebItemData = PROJECTDOC.prerenderWizard(e, state);
      });

	wizard.on("post-render.page1Id", function(e, state) {
        $('#day-date').datepicker({
            dateFormat : "yy-mm-dd"
        });
    });

    wizard.on('submit.page1Id', function(e, state) {
    	PROJECTDOC.adjustToLocation(state);
    });

    wizard.assembleSubmissionObject = PROJECTDOC.assemblePageCreationData;
});

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-devdiary:create-doctype-template-asset',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-devdiary:create-doctype-template-event',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-devdiary:create-doctype-template-todo',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-devdiary:create-doctype-template-vision',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-devdiary:create-doctype-template-annual-vision',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-devdiary:create-doctype-template-cheat',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-devdiary:create-doctype-template-cheat-type',
		PROJECTDOC.standardWizard);