/*
 * Copyright 2014-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function() {
	var macroName = 'projectdoc-devdiary-rating';
    var updateMacro = function(macroNode, rating) {
        AJS.Rte.BookmarkManager.storeBookmark();
        var macroRenderRequest = {
            contentId: Confluence.Editor.getContentId(),
            macro: {
                name: macroName,
                defaultParameterValue: "",
                params: {"value": rating},
                body: ""
            }
        };
        tinymce.confluence.MacroUtils.insertMacro(macroRenderRequest);
    };

    AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-10-perfect", function(e, macroNode) {
        updateMacro(macroNode, "10-perfect");
    });
    AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-9-good-up", function(e, macroNode) {
        updateMacro(macroNode, "9-good-up");
    });
    AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-8-good", function(e, macroNode) {
        updateMacro(macroNode, "8-good");
    });

    AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-7-good-down", function(e, macroNode) {
        updateMacro(macroNode, "7-good-down");
    });
    AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-6-ok-up", function(e, macroNode) {
        updateMacro(macroNode, "6-ok-up");
    });
    AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-five-ok", function(e, macroNode) {
        updateMacro(macroNode, "five-ok");
    });
    AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-4-ok-down", function(e, macroNode) {
        updateMacro(macroNode, "4-ok-down");
    });
    AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-3-imperfect-up", function(e, macroNode) {
        updateMacro(macroNode, "3-imperfect-up");
    });
    AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-2-imperfect", function(e, macroNode) {
        updateMacro(macroNode, "2-imperfect");
    });
    AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-1-imperfect-down", function(e, macroNode) {
        updateMacro(macroNode, "1-imperfect-down");
    });
    AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-zero-unrated", function(e, macroNode) {
        updateMacro(macroNode, "zero-unrated");
    });
})();

(function() {
	var macroName = 'projectdoc-devdiary-shortrating';
    var updateShortMacro = function(macroNode, rating) {
        AJS.Rte.BookmarkManager.storeBookmark();
        var macroRenderRequest = {
            contentId: Confluence.Editor.getContentId(),
            macro: {
                name: macroName,
                defaultParameterValue: "",
                params: {"value": rating},
                body: ""
            }
        };
        tinymce.confluence.MacroUtils.insertMacro(macroRenderRequest);
    };

    AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-4-perfect", function(e, macroNode) {
    	updateShortMacro(macroNode, "4-perfect");
    });
    AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-3-good", function(e, macroNode) {
    	updateShortMacro(macroNode, "3-good");
    });
    AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-2-ok", function(e, macroNode) {
    	updateShortMacro(macroNode, "2-ok");
    });
    AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-1-imperfect", function(e, macroNode) {
    	updateShortMacro(macroNode, "1-imperfect");
    });
    AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-0-unrated", function(e, macroNode) {
    	updateShortMacro(macroNode, "0-unrated");
    });
})();
