/*
 * Copyright 2014-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.devdiary;

import de.smartics.projectdoc.atlassian.confluence.api.doctypes.Doctype;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderHelper;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.user.User;
import com.ibm.icu.text.SimpleDateFormat;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * Provides information for a doctype that requires the date in the title.
 */
public class ProjectDocTimedTitleContextProvider
    extends AbstractProjectDocContextProvider {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The identifier to reference the creation time of the doctype instance.
   */
  public static final String CREATION_TIME =
      "projectdoc.doctype.common.creationTime";

  /**
   * The identifier to reference the current user's full name.
   */
  public static final String USER_FULL_NAME =
      "projectdoc.doctype.common.userFullName";

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  public ProjectDocTimedTitleContextProvider() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  public void setSupport(final ContextProviderSupportService support) {
    super.setSupport(support);
  }

  // --- business -------------------------------------------------------------

  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    final ContextProviderHelper handler = createHandler(blueprintContext);
    addPropertiesToContext(handler);

    final String name =
        ObjectUtils.toString(blueprintContext.get(Doctype.NAME), null);
    if (StringUtils.isNotBlank(name)) {
      final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
      final Date date = (Date) blueprintContext
          .get(ContextProviderHelper.CREATION_DATE_INSTANCE);
      adjustTitle(blueprintContext, name, format, date);
    }

    final User user = AuthenticatedUserThreadLocal.get();
    if (user != null) {
      final String userFullname = user.getFullName();
      blueprintContext.put(USER_FULL_NAME, userFullname);
    }

    updateContextFinally(blueprintContext);

    return blueprintContext;
  }

  private void adjustTitle(final BlueprintContext blueprintContext,
      final String name, final SimpleDateFormat format, final Date date) {
    final String dateString = format.format(date);
    final String title = dateString + ' ' + name;
    blueprintContext.setTitle(title);
    blueprintContext.put(Doctype.SORT_KEY, title);
  }

  // --- object basics --------------------------------------------------------

}
